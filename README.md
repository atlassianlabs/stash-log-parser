# Atlassian Bitbucket Data Center access log parser


The log parser parses and aggregates the access logs of the Atlassian
Bitbucket Data Center application. The main focus is on analyzing
the git operations as they tend to dominate the overall performance of the application.

## Installation

### Download Binaries

There are pre-built binaries for macOS and Linux that can be downloaded from the [Logparser download archive](https://bitbucket.org/atlassianlabs/stash-log-parser/downloads/) page.

Pick the right archive for your operating system.

### Installation on macOS

This is valid for ARM64 (M1) systems. There are no pre-built binary files
for macOS Intel x86-64 systems.

There are pre-requisites to be fulfilled. 

Be sure that you have installed:
* [MacPorts](https://www.macports.org/install.php) for your version of macOS.
* [Homebrew](https://brew.sh/)

* Install Cairo package using MacPorts:
```
sudo /opt/local/bin/port install cairo
```
* Install XQuartz using Brew:
```
brew install xquartz
```
* The .tgz archive contains only `logparser` executable file, and you can unpack it in any
directory you like. The directory `/opt/local/bin` is a good candidate.

### Installation on Linux systems

This is valid for both ARM64 and Intel x86-64 systems.

* To install logparser on destination system, unpack .tgz archive into '/' directory.
Archive contains both executable and all necessary libraries.
```
tar xC / -f logparser-*.tgz
```

### Installation in AISE

Page [Using internal tools (including logparser) in AISE](https://hello.atlassian.net/wiki/spaces/~msuchecki/pages/2821104049/Using+internal+tools+including+logparser+in+AISE) contains
instructions how to install the `logparser` in AISE.


## Usage

The `logparser` binary supports multiple commands and accepts one or more
logfiles as arguments (either in uncompressed or compressed (bzip2) form).

Executing `logparser` without any arguments or with the `--help` argument will
show the help with a list of supported commands:

```
$ logparser --help
logparser 3.2.1

logparser [COMMAND] ... [OPTIONS]
  Logparser for the Atlassian Stash & Bitbucket Server access logs

Commands:
  maxConn                Show the maximum number of concurrent requests per
                         hour
...
...
```

The logparser will parse, analyze and aggregate the log files. It can either print the
aggregated records to STDOUT or it can generate graphs that show the aggregated results.

### Log aggregation

E.g. for the `gitOperations` command that shows the number of git operations
per hour, the output will look like this:

```
[922] λ > logparser gitOperations atlassian-stash-access-2012-*.log.bz2
# Date | clone | fetch | shallow clone | push | ref advertisement ...
2012-08-22 18|2|0|13|0|733|0|0|0|0|0|2|0|13|0|733
2012-08-22 19|3|24|74|0|1660|0|0|0|0|0|3|24|74|0|1660
2012-08-22 20|2|33|119|0|1369|0|0|0|0|0|2|33|119|0|1369
2012-08-22 21|1|12|49|0|1514|0|0|0|0|0|1|12|49|0|1514
```

The fields are `|` separated. The first column usually contains a date field
(using a 60 minute granularity). The format of the remaining columns depends on
the command that is being used.

The first line of the output is a column name header prepended by a '#'.

The output can be used to further analyze the results.

### Chart generation

Most commands accept a `--graph` or `-g` flag that will switch the logparser from printing aggregate results to STDOUT to generating graphs that will be stored in the current working directory (or in the directory specified in the `--target` argument).

## Access log format

The access log format is documented here:
[How to read the Bitbucket Server Log Formats](https://confluence.atlassian.com/display/BitbucketServerKB/How+to+read+the+Bitbucket+Server+Log+Formats)
