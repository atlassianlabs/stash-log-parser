{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Stash.Log.Chart (
    generateGitHostingChart
  , generateGitRefsChart
  , generateGitDurationChart
  , generateRequestClassificationChart
  , generateMaxConnectionChart
  , generateProtocolStats
  , generateRepositoryStats
) where


import           Control.DeepSeq
import           Control.Lens
import qualified Data.ByteString.Char8                  as S
import           Data.Colour
import           Data.Colour.SRGB
import           Data.Colour.Names
import           Data.Default.Class
import           Data.Function                          (on)
import           Data.List                              (sortBy)
import           Data.Time.LocalTime
import           Graphics.Rendering.Chart
import           Graphics.Rendering.Chart.Backend.Cairo
import           Stash.Log.Analyser
import           Stash.Log.Common
import           Stash.Log.GitOpsAnalyser
import           Stash.Log.Types
import           System.Directory                       (createDirectoryIfMissing)
import           System.FilePath                        ((</>))


-- =================================================================================

generateGitHostingChart :: String -> FilePath -> [GitOperationStats] -> IO ()
generateGitHostingChart fileName targetDir xs = do
    -- show all operations in the same graph
    renderChart targetDir (fileName ++ "-0") (gitOpsHostingChart xs)
    -- cache hit/miss per operation type
    mapM_ action [Clone, ShallowClone, Fetch]
  where
    action op' = renderChart targetDir (fileName ++ "-" ++ show op') (gitOpsHostingChart' op' xs)


generateGitRefsChart :: String -> FilePath -> [GitOperationStats] -> IO ()
generateGitRefsChart fileName targetDir xs = do
    -- show all operations in the same graph
    renderChart targetDir (fileName ++ "-0") (gitOpsRefsChart xs)
    -- cache hit/miss per operation type
    mapM_ action [Push, RefAdvertisement]
  where
    action op' = renderChart targetDir (fileName ++ "-" ++ show op') (gitOpsRefsChart' op' xs)


generateGitDurationChart :: String -> FilePath -> [RequestDurationStat] -> IO ()
generateGitDurationChart fileName targetDir xs =
    mapM_ action [Clone, ShallowClone, Push, Fetch, RefAdvertisement]
  where
    action op' = renderChart targetDir (fileName ++ "-" ++ show op') (gitDurationChart op' xs)

generateRequestClassificationChart :: String -> FilePath -> RequestClassification -> IO ()
generateRequestClassificationChart fileName targetDir RequestClassification{..} = do
    let xs =  [
               ("Git HTTP", gitHttp)
             , ("Git SSH", gitSsh)
             , ("Web UI", webUi)
             , ("File Server", fileServer)
             , ("REST", rest)
            ]
    renderChart targetDir fileName (pieChart "Distribution of operations" xs)


generateMaxConnectionChart :: String -> FilePath -> [DateValuePair] -> IO ()
generateMaxConnectionChart fileName targetDir xs = renderChart targetDir fileName (stackedWithLinesChart "Concurrent Connections" ([]::[Line Double]) $ toLines xs)
  where
    toLines xs' = [Line ALeft "max concurrent connections" (fmap (\DateValuePair{..} -> (toLocalTime getLogDate, fromIntegral getValue)) xs') blue]

generateProtocolStats :: String -> FilePath -> [ProtocolStats] -> IO ()
generateProtocolStats fileName targetDir xs = renderChart targetDir fileName (linesChart "Git protocol usage" (toLines xs))
  where
    toLines :: [ProtocolStats] -> [Line Double]
    toLines xs' = [
        Line ALeft "HTTP(S)" (fmap (\ProtocolStats{..} -> (toLocalTime getProtocolLogDate, fromIntegral getHttp)) xs') p2tan
      , Line ALeft "SSH" (fmap (\ProtocolStats{..} -> (toLocalTime getProtocolLogDate, fromIntegral getSsh)) xs') p2turquoise
      ]


generateRepositoryStats :: String -> FilePath -> [RepositoryStat] -> IO ()
generateRepositoryStats fileName targetDir xs = do
    let ys = fmap (\RepositoryStat{..} -> (S.unpack getName ++ " (" ++ show getNumberOfClones ++ ")", fromIntegral getNumberOfClones)) $ take 10 $
             sortBy (flip (compare `on` getNumberOfClones)) (filter statAvailable xs)
    renderChart targetDir fileName (pieChart "Top 10 repositories by number of clones" ys)
  where
    statAvailable StatUnavailable = False
    statAvailable _               = True

-- =================================================================================

data OperationType = Clone | ShallowClone | Fetch | Push | RefAdvertisement deriving (Eq, Show)

data Cache = Hit | Miss deriving (Eq, Show)

type Data a = [(LocalTime,a)]

data Anchor = ALeft | ARight deriving (Eq, Show)

data Line a = Line {
    anchor         :: !Anchor
  , lbl            :: !S.ByteString
  , lineDataPoints :: Data a
  , color          :: Colour Double
} deriving (Eq, Show)

instance (Num a, NFData a) => NFData (Line a) where
    rnf l@Line{..} =
        l {
            lineDataPoints = lineDataPoints `deepseq` lineDataPoints
          , color = color `seq` color
        } `seq` ()

instance NFData LogValue where
    rnf (LogValue d) = d `seq` ()


gitDurationChart :: OperationType -> [RequestDurationStat] -> Renderable ()
gitDurationChart op' xs =
        pointChart "Duration of Git Operations (in seconds)" (toLines op' xs)
  where
     toLines _ []                 = []
     toLines Clone ys             = [Line ALeft "clone/cache hit"              (f cacheHitDurations Clone ys) p1yellow
                                   , Line ALeft "clone/cache miss"             (f cacheMissDurations Clone ys) p1blue]
     toLines ShallowClone ys      = [Line ALeft "shallow clone/cache hit"      (f cacheHitDurations ShallowClone ys) p2tan
                                   , Line ALeft "shallow clone/cache miss"     (f cacheMissDurations ShallowClone ys) p2turquoise]
     toLines Push ys              = [Line ALeft "push"                         (extractPushes ys) p3blue]
     toLines Fetch ys             = [Line ALeft "fetch"                        (f cacheMissDurations Fetch ys) p4pink]
     toLines RefAdvertisement ys  = [Line ALeft "ref advertisement"            (f cacheMissDurations RefAdvertisement ys) p4yellow]
     extractPushes                = fmap (\RequestDurationStat{..} -> (toLocalTime getDurationDate, toLogValue (cacheHitDurations !! 3 + cacheMissDurations !! 3)))
     f fieldF                     = extractField (\x r@RequestDurationStat{..} -> (toLocalTime getDurationDate, toLogValue (fieldF r !! x)))
     toLogValue                   = LogValue . toSeconds


extractField :: (Int -> a -> (LocalTime, b)) -> OperationType -> [a] -> [(LocalTime, b)]
extractField f ot xs
    | ot == Clone            = fmap (f 0) xs
    | ot == ShallowClone     = fmap (f 2) xs
    | ot == Fetch            = fmap (f 1) xs
    | ot == RefAdvertisement = fmap (f 4) xs
    | ot == Push             = fmap (f 3) xs
    | otherwise              = []


gitOpsHostingChart :: [GitOperationStats] -> Renderable ()
gitOpsHostingChart xs =
    stackedWithLinesChart "Overview of Git [scm-hosting] Operations" (refLines xs) (toLines xs)
  where
    toLines []               = []
    toLines ys               = [
                                   Line ALeft "clone" (f Clone ys) oiDarkOrange
                                 , Line ALeft "fetch" (f Fetch ys) oiGreen
                                 , Line ALeft "shallow clone" (f ShallowClone ys) oiBlack
                               ]
    refLines []              = []
    refLines _               = []
    f                        = extractField (\x GitOperationStats{..} -> (toLocalTime getOpStatDate, toDouble (cacheHits !! x + cacheMisses !! x)))

gitOpsHostingChart' :: OperationType -> [GitOperationStats] -> Renderable ()
gitOpsHostingChart' op' xs =
    stackedWithLinesChart ("Git [scm-hosting] Operation: " ++ show op') (sumLines op' xs) (toLines op' xs)
  where
    toLines _ []                = []
    toLines Clone ys            = [Line ALeft "clone/cache hit" (f cacheHits Clone ys) p1yellow
                                 , Line ALeft "clone/cache miss" (f cacheMisses Clone ys) p1blue]
    toLines ShallowClone ys     = [Line ALeft "shallow clone/cache hit" (f cacheHits ShallowClone ys) p2tan
                                 , Line ALeft "shallow clone/cache miss" (f cacheMisses ShallowClone ys) p2turquoise]
    toLines Push _              = []
    toLines Fetch ys            = [Line ALeft "fetch" (f cacheMisses Fetch ys) oiGreen]
    toLines RefAdvertisement _  = []

    sumLines Clone ys            = [Line ALeft "clone total" (f cacheHits Clone  ys `add` f cacheMisses Clone ys) p3blue]
    sumLines ShallowClone ys     = [Line ALeft "shallow clone total" (f cacheHits ShallowClone ys `add` f cacheMisses ShallowClone ys) p3pink]
    sumLines Push _              = []
    sumLines Fetch _             = []
    sumLines RefAdvertisement _  = []

    add xs' ys                   = let zs = zip xs' ys
                                   in fmap (\((lk,lv),(_,rv)) -> (lk, lv + rv)) zs

    f fieldF                     = extractField (\x r@GitOperationStats{..} -> (toLocalTime getOpStatDate, toDouble (fieldF r !! x)))


gitOpsRefsChart :: [GitOperationStats] -> Renderable ()
gitOpsRefsChart xs =
    stackedWithLinesChart "Overview of Git [scm-refs] Operations" (refLines xs) (toLines xs)
  where
    toLines []               = []
    toLines ys               = [Line ALeft "push" (f Push ys) p3pink]
    refLines []              = []
    refLines ys              = [Line ARight "ref advertisement" (f RefAdvertisement ys) p3blue]
    f                        = extractField (\x GitOperationStats{..} -> (toLocalTime getOpStatDate, toDouble (cacheHits !! x + cacheMisses !! x)))

gitOpsRefsChart' :: OperationType -> [GitOperationStats] -> Renderable ()
gitOpsRefsChart' op' xs =
    stackedWithLinesChart ("Git [scm-refs] Operation: " ++ show op') (sumLines op' xs) (toLines op' xs)
  where
    toLines _ []               = []
    toLines Clone _            = []
    toLines ShallowClone _     = []
    toLines Push ys            = [Line ALeft "push" (extractPushes ys) p3pink]
    toLines Fetch _            = []
    toLines RefAdvertisement ys = [Line ALeft "ref advertisement" (f cacheMisses RefAdvertisement ys) p3blue]

    sumLines Clone _            = []
    sumLines ShallowClone _     = []
    sumLines Push _             = []
    sumLines Fetch _            = []
    sumLines RefAdvertisement _ = []

    extractPushes                = fmap (\GitOperationStats{..} -> (toLocalTime getOpStatDate, toDouble (cacheHits !! 3 + cacheMisses !! 3)))
    f fieldF                     = extractField (\x r@GitOperationStats{..} -> (toLocalTime getOpStatDate, toDouble (fieldF r !! x)))


pieChart :: String -> [(String, Integer)] -> Renderable ()
pieChart title values = toRenderable layout
  where
    pitem (s,v) = pitem_value .~ fromIntegral v
                  $ pitem_label .~ s
                  $ pitem_offset .~ 0
                  $ def

    layout = pie_title .~ title
           $ pie_title_style .~ (font_size .~ 24 $ def)
           $ pie_plot . pie_data .~ map pitem values
           $ pie_plot . pie_colors  .~ fmap opaque colors
           $ pie_plot . pie_label_style .~ (font_size .~ 16 $ def)
           $ pie_margin .~ 106.0
           $ def


pointChart :: String -> [Line LogValue] -> Renderable ()
pointChart title lines' = do
    let plots = (\Line{..} -> toPlot $ points color lbl lineDataPoints) <$> lines' -- :: [(Plot LocalTime LogValue)]
    toRenderable (layout plots)
  where
    layout p = layout_title .~ title
           $ layout_title_style .~ (font_size .~ 24 $ def)
           $ layout_background .~ solidFillStyle (opaque gray98)
           $ layout_left_axis_visibility . axis_show_ticks .~ True
           $ layout_foreground .~ (opaque gray16) 
           $ layout_plots .~ p
           $ def

    {-points :: Colour LogValue -> String -> Data LogValue -> PlotPoints LocalTime LogValue-}
    points col title' dat = plot_points_style .~ filledCircles 1 (opaque col)
           $ plot_points_values .~ dat
           $ plot_points_title .~ (S.unpack title')
           $ def

linesChart :: (PlotValue a) => String -> [Line a] -> Renderable ()
linesChart title lines' = do
    let plots = (\Line{..} -> toPlot $ single color lbl lineDataPoints) <$> lines' -- :: [(Plot LocalTime LogValue)]
    toRenderable (layout plots)
  where
    single col title' dat = plot_lines_style .~ lineStyle col
           $ plot_lines_values .~ [dat]
           $ plot_lines_title .~ (S.unpack title')
           $ def

    layout p = layout_title .~ title
           $ layout_title_style .~ (font_size .~ 24 $ def)
           $ layout_background .~ solidFillStyle (opaque gray98)
           $ layout_plots .~ p
           $ layout_foreground .~ (opaque gray16)
           $ def

    lineStyle col = line_width .~ 2
              $ line_color .~ opaque col
              $ def

stackedWithLinesChart :: (Num a, PlotValue a) => String -> [Line a] -> [Line a] -> Renderable ()
stackedWithLinesChart title singleLines' lines' = do
    let stacked  = toEither line <$> lines'
        single'  = toEither single <$> singleLines'
        xs       = single' ++ stacked
        hasRight = any isRight xs
    toRenderable (layout xs hasRight)
  where
    line col title' dat = plot_fillbetween_style .~ solidFillStyle (col `withOpacity` 0.6)
           $ plot_fillbetween_values .~ [ (d,(0,v)) | (d,v) <- dat]
           $ plot_fillbetween_title .~
            (S.unpack title')
           $ def
    isRight (Left _)  = False
    isRight (Right _) = True
    single col title' dat = plot_lines_style .~ lineStyle col
           $ plot_lines_values .~ [dat]
           $ plot_lines_title .~ (S.unpack title')
           $ def

    layout p showRight = layoutlr_title .~ title
           $ layoutlr_title_style .~ (font_size .~ 24 $ def)
           $ layoutlr_background .~ solidFillStyle (opaque gray98)
           $ layoutlr_left_axis_visibility . axis_show_ticks .~ False
           $ layoutlr_right_axis_visibility . axis_show_line .~ showRight
           $ layoutlr_right_axis_visibility . axis_show_ticks .~ False
           $ layoutlr_right_axis_visibility . axis_show_labels .~ showRight
           $ layoutlr_foreground .~ (opaque gray16)
           $ layoutlr_plots .~ p
           $ def

    lineStyle col = line_width .~ 2
              $ line_color .~ opaque col
              $ def

toEither :: ToPlot a => (Colour Double -> S.ByteString -> Data t -> a x y)
                  -> Line t -> Either (Plot x y) (Plot x y)
toEither f (Line ALeft lbl points col) = Left (toPlot $ f col lbl points)
toEither f (Line ARight lbl points col) = Right (toPlot $ f col lbl points)



renderChart :: FilePath -> String -> Renderable a -> IO ()
renderChart targetDir filename rend = do
    createDirectoryIfMissing True targetDir
    {-_ <- renderableToFile (FileOptions (1200,800) PNG) rend (targetDir </> filename ++ ".png")-}
    _ <- renderableToFile (FileOptions (1200,800) PDF) (targetDir </> filename ++ ".pdf") rend
    return ()


colors :: [Colour Double]
colors = cycle [oiBlack, oiGreen, oiBlue, oiCyan, oiYellow, oiOrange, oiDarkOrange, oiPink]

-- color pairs from https://www.nceas.ucsb.edu/sites/default/files/2022-06/Colorblind%20Safe%20Color%20Schemes.pdf
p1yellow, p1blue, p2tan, p2turquoise, p3blue, p3pink, p4yellow, p4pink :: Colour Double

p1yellow = sRGB' 253 179 56
p1blue = sRGB' 2 81 150

p2tan = sRGB' 255 190 106
p2turquoise = sRGB' 64 176 166

p3blue = sRGB' 16 85 154
p3pink = sRGB' 219 76 119

p4yellow = sRGB' 244 179 1
p4pink = sRGB' 219 16 72

--p5orange = sRGB' 235 97 35
--p5purple = sRGB' 81 40 136

-- color blindness Okabe and Itto palette from https://www.nceas.ucsb.edu/sites/default/files/2022-06/Colorblind%20Safe%20Color%20Schemes.pdf
oiBlack, oiGreen, oiBlue, oiCyan, oiYellow, oiOrange, oiDarkOrange, oiPink :: Colour Double

oiBlack = sRGB' 0 0 0
oiGreen = sRGB' 0 158 115
oiBlue = sRGB' 0 114 178
oiCyan = sRGB' 86 180 233
oiYellow = sRGB' 240 228 66
oiOrange = sRGB' 230 159 0
oiDarkOrange = sRGB' 213 94 0
oiPink = sRGB' 204 121 167

gray98 :: Colour Double
gray98 = sRGB' 250 250 250

gray16 :: Colour Double
gray16 = sRGB' 43 43 43

sRGB' :: Double -> Double -> Double -> Colour Double
sRGB' a b c = sRGB (toR a) (toR b) (toR c)

toR :: Double -> Double
toR i = i/255.0

toDouble :: Int -> Double
toDouble = (1.0 *) . fromIntegral

-- |
--
-- >>> toSeconds (Millis 1200)
-- 1.2
toSeconds :: Millis -> Double
toSeconds = (/ 1000.0) . fromIntegral . millis
