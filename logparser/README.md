# Information

General information on updating and recompiling Logparser is available on page [Recompiling Atlassian Bitbucket Server Access Log Parser](https://hello.atlassian.net/wiki/spaces/~6421cfde6b29c052ab2edd43/pages/2774216814/Recompiling+Atlassian+Bitbucket+Server+Access+Log+Parser)

# Build

## Pre-requisites


### Ubuntu Linux

On Ubuntu Linux, make sure you have the following packages installed:
```
make
pkg-config
haskell-stack
happy
alex
libnuma1
libbz2-dev
libnuma-dev
zlib1g-dev
libcairo2-dev
```

Upgrade of the `stack` tool may not be strictly necessary but by upgrading it we are making sure old version will not cause any problem.

* Upgrade `stack` on  Intel x86-64 systems:
```
stack upgrade
```
* In-place binary upgrade of `stack` tool may fail on ARM64, so it must be done manually - be aware that this will overwrite system's `stack` command:
```
curl -sSL https://get.haskellstack.org/ | sh -s - -f
```

### macOS

On macOS, make sure you have installed:

* [MacPorts](https://www.macports.org/install.php) for your version of macOS
* [Homebrew](https://brew.sh/)

Install necessary packages:
* `cairo`:
```
sudo /opt/local/bin/port install cairo
```
* Install `haskell-stack` and `llvm` version 12:
```
brew install haskell-stack
brew install llvm@12
```
* Make sure `/opt/homebrew/opt/llvm@12/bin` is in the path - you may want to add this to the `~/.*rc` file for your shell and open a new terminal window, or execute this in existing one before running `make`
```
export PATH="$PATH:/opt/homebrew/opt/llvm@12/bin"
```

## Build the tool

Be sure that you have decent amount of RAM, and plenty of disk space. If you do not have enough RAM, build procedure
will crash leaving behind unusable, half-compiled libraries. Environment setup procedure will download Haskell compiler and various
tools, and it will take several GB of your disk space. Say, 8GB RAM and 10GB free space should be fine.

### Ubuntu Linux

To setup the environment, build the logparser, and produce .tgz archive, run:
```
make all
```

### macOS

Make procedure on macOS is a bit weird. The first time we start the procedure, it will build various libraries needed for the final executable. However, once almost all is done and the `logparser` built is started, it will fail with the message like

```
Linking .stack-work/dist/aarch64-osx/ghc-8.10.7/build/logparser/logparser ...
ld: warning: ignoring duplicate libraries: '-lz'
Undefined symbols for architecture arm64:
  "_iconv", referenced from:
      _hs_iconv in libHSbase-4.14.3.0.a[253](iconv.o)
  "_iconv_close", referenced from:
      _hs_iconv_close in libHSbase-4.14.3.0.a[253](iconv.o)
  "_iconv_open", referenced from:
      _hs_iconv_open in libHSbase-4.14.3.0.a[253](iconv.o)
ld: symbol(s) not found for architecture arm64
clang: error: linker command failed with exit code 1 (use -v to see invocation)
`gcc' failed in phase `Linker'. (Exit code: 1)

Error: [S-7282]
       Stack failed to execute the build plan.

       While executing the build plan, Stack encountered the error:

       [S-7011]
       While building package logparser-3.2.1 (scroll up to its section to see the error) using:
       /Users/nopsenica/.stack/setup-exe-cache/aarch64-osx/Cabal-simple_DY68M0FN_3.2.1.0_ghc-8.10.7 --verbose=1 --builddir=.stack-work/dist/aarch64-osx/ghc-8.10.7 build exe:logparser --ghc-options " -fdiagnostics-color=always"
       Process exited with code: ExitFailure 1
make: *** [build] Error 1

```

To solve it, we need to temporarily deactivate `libiconv` package from 'ports', then re-run `make` which will re-enable it:

```
# deactivate libiconv
sudo /opt/local/bin/port deactivate libiconv

# build logparser, it will reactivate libiconv
make all

```


## Installation

Result of build procedure will be .tgz archive which can be used to distribute compiled tool.
To install it locally from the same .tgz archive, follow the section
"Instalation on ..." for your operating system on the [Atlassian Bitbucket Data Center access log parser](https://bitbucket.org/atlassianlabs/stash-log-parser/src/) page.

# Tests

To run the tests, simply run:
```
make test
```

# Access log notes

If the clone cache plugin is installed, additional information will be
available in the labels section of the access log line.

Since version 1.1.2 of the SCM cache, the cache plugin adds whether the response was a cache hit
or a cache miss.

